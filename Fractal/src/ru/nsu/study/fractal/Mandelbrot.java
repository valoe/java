package ru.nsu.study.fractal;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.IOException;

public class Mandelbrot extends IterativeFractals{
    public Mandelbrot(int width, int height){
        super(width, height);
        this.x = -0.5;
        this.y = 0.0;
        this.scale = 50000.0;
        this.deep = 200;
    }

    @Override
    public void update() {
        double[] curCoord = new double[2];
        double[] iterCoord = new double[3];
        double modCoord;
        for(int i = 0; i < width; i++){
            for(int j = 0; j < height; j++){
                curCoord = getCoords(i, j);
                iterCoord[0] = curCoord[0];
                iterCoord[1] = curCoord[1];
                iterCoord[2] = iterCoord[0];
                pixelDeep[i][j] = -1;
                for(int d = 0; d < deep; d++){
                    iterCoord[0] = (iterCoord[0] * iterCoord[0]) - (iterCoord[1] * iterCoord[1]) + curCoord[0];
                    iterCoord[1] = 2 * (iterCoord[1] * iterCoord[2]) + curCoord[1];
                    iterCoord[2] = iterCoord[0];
                    modCoord = (iterCoord[0] * iterCoord[0]) + (iterCoord[1] * iterCoord[1]);
                    if (modCoord > 4){
                        pixelDeep[i][j] = d;
                        break;
                    }
                }
            }
        }
    }
}
