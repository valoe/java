package ru.nsu.study.fractal;

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;

public abstract class IterativeFractals implements Fractal{
    protected double x, y;
    protected double scale;
    protected int deep;
    protected int width, height;
    protected int[][] pixelDeep;

    public IterativeFractals(int width, int height){
        this.height = height;
        this.width = width;
        pixelDeep = new int[width][height];
    }

    public abstract void update();

    public void setCenter(double x, double y){
        this.x = x;
        this.y = y;
    }

    public void setScale(double scale){
        this.scale = scale;
    }

    public void setDeep(int deep){
        this.deep = deep;
    }

    public double getCenterX(){
        return x;
    }

    public double getCenterY(){
        return y;
    }

    public double getScale(){
        return scale;
    }

    public int getDeep(){
        return deep;
    }

    public int[][] getPixelDeep(){
        return pixelDeep;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    protected double[] getCoords(int x, int y){
        double step = width / scale;
        double[] coords = new double[2];

        coords[0] = (double)(this.x - step * (width / 2) + x * step);
        coords[1] = (double)(this.y + step * (height / 2) - y * step);

        return coords;
    }
}
