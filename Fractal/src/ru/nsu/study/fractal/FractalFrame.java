package ru.nsu.study.fractal;

import javax.swing.*;
import javax.swing.event.MouseInputAdapter;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.awt.image.BufferedImage;

public class FractalFrame extends JFrame {
    protected IterativeFractals fractal;
    protected Colorization color;
    protected int width, height;
    protected int startX, startY;
    protected BufferedImage image;
    protected JPanel panel;
    private int step = 200;
    private double zoom = 1.05;

    MouseWheelListener mouseWheel;
    MouseInputAdapter mouseInput;

    public FractalFrame(int width, int height){
        super("Fractal");
        this.fractal = new Mandelbrot(width, height);
        this.color = new Colorization(fractal);

        this.width = width;
        this.height = height;

        mouseWheel = new MouseWheelListener() {
            @Override
            public void mouseWheelMoved(MouseWheelEvent e) {
                int scroll=e.getWheelRotation();
                double scale = fractal.getScale();
                if (scroll < 0){
                    scale *= (-1) * scroll * zoom;
                }
                else{
                    scale *= scroll / zoom;
                }

                fractal.setScale(scale);
                paintFractal();
            }
        };

        mouseInput = new MouseInputAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                //super.mousePressed(e);
                Point p = e.getPoint();
                startX = p.x;
                startY = p.y;
            }

            @Override
            public void mouseDragged(MouseEvent e) {
                //super.mouseDragged(e);
                int curX = e.getX();
                int curY = e.getY();
                double x = fractal.getCenterX();
                double y = fractal.getCenterY();
                x += (-1) * step * (curX - startX) / fractal.getScale();
                y += step * (curY - startY) / fractal.getScale();
                fractal.setCenter(x, y);
                startX = curX;
                startY = curY;
                paintFractal();
            }
        };

        panel = new JPanel() {
            @Override
            protected void paintComponent(Graphics g) {
                super.paintComponent(g);
                g.drawImage(image, 0, 0, null);
            }
        };

        this.addMouseListener(mouseInput);
        this.addMouseMotionListener(mouseInput);
        this.addMouseWheelListener(mouseWheel);

        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setSize(fractal.getWidth(), fractal.getHeight());

        paintFractal();
        this.add(panel);
        this.setVisible(true);
    }

    private void paintFractal(){
        fractal.update();
        image = color.getImage();
        repaint();
    }
}
