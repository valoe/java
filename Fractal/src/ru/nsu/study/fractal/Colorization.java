package ru.nsu.study.fractal;

import java.awt.image.BufferedImage;

public class Colorization {
    protected int palette;
    protected BufferedImage image;
    protected IterativeFractals fractal;
    protected int[][] pixelDeep;

    public Colorization(IterativeFractals fractal){
        palette = 0;
        this.fractal = fractal;
        image = new BufferedImage(fractal.getWidth(), fractal.getWidth(), BufferedImage.TYPE_INT_RGB);
    }

    public void setPalette(int palette){
        this.palette = palette;
    }

    public BufferedImage getImage(){
        colorize();
        return image;
    }

    private void colorize(){
        pixelDeep = fractal.getPixelDeep();
        switch (palette){
            case 0:
                simpleColorize();
                break;

            default:
                System.out.println("Bad palette");
                break;
        }
    }

    private void simpleColorize(){
        for(int i = 0; i < fractal.getWidth(); i++) {
            for (int j = 0; j < fractal.getHeight(); j++) {
                if (pixelDeep[i][j] < 0){
                    image.setRGB(i, j, 0);
                }
                else{
                    image.setRGB(i, j, pixelDeep[i][j] * 11 + 40);
                }
            }
        }
    }
    private void someColorize(){
        for(int i = 0; i < fractal.getWidth(); i++) {
            for (int j = 0; j < fractal.getHeight(); j++) {
                if (pixelDeep[i][j] < 0){
                    image.setRGB(i, j, 0);
                }
                else{
                    int color_r = ((pixelDeep[i][j] >> 6) * 5) << 18;
                    int color_g = (((pixelDeep[i][j] % 64) >> 3) * 5) << 10;
                    int color_b = ((pixelDeep[i][j] % 8) * 5) << 2;
                    int color = color_r + color_g + color_b;
                    image.setRGB(i, j, color);
                }
            }
        }
    }
}
