package ru.nsu.study.lab1;

import java.io.*;
import java.util.*;
import java.util.stream.Collectors;

public class Main{
    public static void main(String[] args){
        Reader reader = null;
        if (args.length < 1)
            return;
        try{
            reader = new InputStreamReader(new FileInputStream(args[0]));
            int a;
            int wordsNum = 0;
            StringBuilder sb = new StringBuilder();
            HashMap<String, Integer> wordMap = new HashMap<>();
            while((a = reader.read()) >= 0){
                if (Character.isLetterOrDigit(a)){
                    sb.insert(0, (char)a);
                }
                else {
                    if (sb.length() > 1){
                        wordsNum++;
                        sb.reverse();
                        if (wordMap.containsKey(sb.toString())){
                            int i = wordMap.get(sb.toString());
                            i++;
                            wordMap.replace(sb.toString(), i);
                        }else{
                            wordMap.put(sb.toString(), 1);
                        }
                        sb.delete(0, sb.length());
                    }
                }
            }
            {
                if (sb.length() > 1){
                    wordsNum++;
                    sb.reverse();
                    if (wordMap.containsKey(sb.toString())){
                        int i = wordMap.get(sb.toString());
                        i++;
                        wordMap.replace(sb.toString(), i);
                    }else{
                        wordMap.put(sb.toString(), 1);
                    }
                    sb.delete(0, sb.length());
                }
            }
            Map<String, Integer> sortedMap = wordMap.entrySet().stream()
                    .sorted(Collections.reverseOrder(Map.Entry.comparingByValue()))
                    .collect(Collectors.toMap(
                            Map.Entry::getKey,
                            Map.Entry::getValue,
                            (e1, e2) -> e1,
                            LinkedHashMap::new));
            for(Map.Entry entry: sortedMap.entrySet()){
                int j = (int)entry.getValue();
                System.out.println(entry.getKey() + ":" + j + ":" + ((j * 100)/wordsNum) + "%");
            }
        } catch (IOException e) {
            System.err.println("Error while reading file: " + e.getLocalizedMessage());
        }
        finally {
            if (null != reader){
                try{
                    reader.close();
                }
                catch(IOException e){
                    e.printStackTrace(System.err);
                }
            }

        }
    }
}

